import mpsd_hpc_tools.lib as lib


def test_humanise_size() -> None:
    assert lib.humanise_size(10) == " 10.00 B"
    assert lib.humanise_size(999) == "999.00 B"
    assert lib.humanise_size(1000) == "  1.00 KB"
    assert lib.humanise_size(10000) == " 10.00 KB"
    assert lib.humanise_size(22000) == " 22.00 KB"
    assert lib.humanise_size(330000) == "330.00 KB"
    assert lib.humanise_size(22000000) == " 22.00 MB"
    assert lib.humanise_size(25 * 10**12) == " 25.00 TB"
