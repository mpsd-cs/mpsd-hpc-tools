"""Tests for mpsd-quota command.

We use mocking to just test the functions we have written. The test return
values of the mocked function are taken from actual system calls on the MPSD
HPC system.

The complexity of these functions is not very high; perhaps the testing is not
necessary as such. However, the motivation is to create a framework in which
tests can easily be added if desired.

"""


import pathlib
import mpsd_hpc_tools.quota as quota


def test_get_ceph_attribute(mocker):
    mock = mocker.patch("subprocess.check_output", return_value=b"25000000000000")
    assert (
        quota.get_ceph_attribute("scratch/MOCK", "ceph.quota.max_bytes")
        == 25000000000000
    )
    mock.assert_called_with(
        [
            "getfattr",
            "-n",
            "ceph.quota.max_bytes",
            "--only-values",
            "--absolute-names",
            "scratch/MOCK",
        ]
    )


def test_scratch_bytes_used_quota(mocker):
    # every call of subprocess.check_output returns 2500 bytes:
    mock = mocker.patch(
        "mpsd_hpc_tools.quota.get_ceph_attribute", return_value=10241024
    )

    used, avail = quota.scratch_bytes_used_quota("/scratch/MOCK")
    assert isinstance(used, int)
    assert isinstance(avail, int)
    assert used == 10241024
    assert avail == 10241024
    assert len(mock.call_args_list) == 2
    assert mock.call_args_list[0].args == ("/scratch/MOCK", "ceph.dir.rbytes")
    assert mock.call_args_list[1].args == ("/scratch/MOCK", "ceph.quota.max_bytes")


def test_get_df_output(mocker):
    mock = mocker.patch(
        "subprocess.check_output", return_value=b"     Used\n4553966kB\n"
    )
    assert quota.get_df_output("/home/MOCK", "used") == 4553966 * 1000
    mock.assert_called_once()
    mock.assert_called_with("df --output=used --block-size=KB /home/MOCK".split())


def test_home_bytes_used_quota(mocker):
    mock = mocker.patch("mpsd_hpc_tools.quota.get_df_output", return_value=1000)

    used, avail = quota.home_bytes_used_quota("/home/MOCK")
    assert isinstance(used, int)
    assert isinstance(avail, int)
    assert used == 1000
    assert avail == 1000
    assert len(mock.call_args_list) == 2
    assert mock.call_args_list[0].args == ("/home/MOCK", "used")
    assert mock.call_args_list[1].args == ("/home/MOCK", "avail")


def test_compose_quota_report(mocker):
    #
    # 1. human readable units (bytes=False)
    #
    mocker.patch(
        "mpsd_hpc_tools.quota.home_bytes_used_quota",
        return_value=(1_000_000, 5_000_000_000),
    )
    mocker.patch(
        "mpsd_hpc_tools.quota.scratch_bytes_used_quota",
        return_value=(3_000, 25_000_000_000_000),
    )
    output = quota.compose_quota_report(
        homedir="/home/username",
        bytes=False,
        scratchdir=pathlib.Path("/scratch/username"),
    )
    output_lines = output.splitlines()

    # Expect output like this:
    """
    location                       used          avail            use%
    /home/username              1.00 MB        5.00 GB           0.02%
    /scratch/username           3.00 KB       25.00 TB        1.2e-08%
    """
    print(output)  # for debugging of test

    assert "1.00 MB" in output_lines[1]
    assert "/home/username" in output_lines[1]
    assert "5.00 GB" in output_lines[1]
    assert "/scratch/username" in output_lines[2]
    assert "25.00 TB" in output_lines[2]
    assert "3.00 KB" in output_lines[2]
    # in total, we expect three lines
    assert len(output_lines) == 3

    #
    # 2. human readable units (bytes=False)
    #
    output = quota.compose_quota_report(
        homedir=pathlib.Path("/home/username"),
        bytes=True,
        scratchdir="/scratch/username",
    )
    output_lines = output.splitlines()
    # Expect output like this:
    """
    location                       used          avail            use%
    /home/username              1000000     5000000000           0.02%
    /scratch/username              3000 25000000000000        1.2e-08%
    """

    print(output)  # for debugging of test
    assert "1000000" in output_lines[1]
    assert "/home/username" in output_lines[1]
    assert "5000000000" in output_lines[1]
    assert "0.02%" in output_lines[1]
    assert "/scratch/username" in output_lines[2]
    assert "25000000000000" in output_lines[2]
    assert "3000" in output_lines[2]
    assert "1.2e-08%" in output_lines[2]
    # in total, we expect three lines
    assert len(output_lines) == 3


def test_compose_quota_report_zero_avail(mocker):
    """Want to test if division by zero occurs if 0 bytes are available.

    Was a problem in an earlier version
    (see https://gitlab.gwdg.de/mpsd-cs/mpsd-hpc-tools/-/issues/7)
    """

    mocker.patch(
        "mpsd_hpc_tools.quota.home_bytes_used_quota",
        return_value=(5_000_000_000, 0),
    )
    mocker.patch(
        "mpsd_hpc_tools.quota.scratch_bytes_used_quota",
        return_value=(25_000_000_000_000, 0),
    )
    output = quota.compose_quota_report(
        homedir="/home/username",
        bytes=False,
        scratchdir=pathlib.Path("/scratch/username"),
    )
    print("\n" + output)  # debug
    output_lines = output.splitlines()

    # Expect output like this:
    """
    location                       used          avail            use%
    /home/username              5.00 GB         0.00 B          100.0%
    /scratch/username          25.00 TB         0.00 B          100.0%
    """

    assert "0.00 B" in output_lines[1]
    assert "5.00 GB" in output_lines[1]
    assert "100.0%" in output_lines[1]
    assert "/scratch/username" in output_lines[2]
    assert "0.00 B" in output_lines[2]
    assert "25.00 TB" in output_lines[2]
    assert "100.0%" in output_lines[2]


def test_mpsd_quota_command_help(capsys):
    try:
        quota.main(["mpsd-quota", "--help"])
    except SystemExit:
        pass
    output = capsys.readouterr().out
    print(f"{output=}")
    assert "Shows current usage of /home and /scratch" in output
    assert "Output of mpsd-quota is in" in output
