MPSD HPC Tools
==============

Collection of tools that are useful on the MPSD High Performance Compute cluster.

The following commands are provided:

``mpsd-quota`` - Show current data usage on ``/home`` and ``/scratch``

Use ``mpsd-quota --help`` to learn about the tool.


Installation
------------

If the software is not provided yet, use the following:

- to install, run ``pipx install git+https://gitlab.gwdg.de/mpsd-cs/mpsd-hpc-tools``

- to update, run ``pipx upgrade mpsd-hpc-tools``

- to uninstall, run ``pipx uninstall mpsd-hpc-tools``


Developers
----------

See `development.rst <development.rst>`__ for instructions for developers.

