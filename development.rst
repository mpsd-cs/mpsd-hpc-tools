Executing the tests
-------------------

Execution of tests should be possible using ``py.test`` in the root directory of the repository or in the ``tests`` subdirectory::

  (venv) fangohr@mpsd-hpc-login2:~/git/mpsd-hpc-tools$ py.test
  ==================================== test session starts ===========
  platform linux -- Python 3.9.13, pytest-7.3.0, pluggy-1.0.0
  rootdir: /home/fangohr/git/mpsd-hpc-tools
  configfile: pyproject.toml
  collected 3 items

  tests/test_lib.py .                                           [ 33%]
  tests/test_quota.py ..                                        [100%]

  ===================================== 3 passed in 0.04s ============
  (venv) fangohr@mpsd-hpc-login2:~/git/mpsd-hpc-tools$

Execution of the tests should be possible straight after cloning the repository,
i.e. installation (via pip or flit) is not required. However, execution of the
tests without installation only works for more recent versions of pytest: works
with pytest 7.3 but does not work with 6.0.


Installation for development
----------------------------

We use `flit <https://flit.pypa.io/en/stable/>`__ for this package (pushing to PyPI, building the wheel).


Short version of instructions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To install for development::

  flit install -s --deps all

This installs the package with symlinks (``-s``) into the current environment.

More detailed steps
~~~~~~~~~~~~~~~~~~~

If necessary, install flit first. Suggested steps:

- create virtual env based on existing python interpreter::

    python3 -m venv venv

- activate virtual environment::

    source venv/bin/activate

- install flit

    pip3 install flit

- install mpsd-hpc-tools in editable mode (will automatically install the
  mpsd-hpc-tools dependencies for development)::

    git clone git@gitlab.gwdg.de:mpsd-cs/mpsd-hpc-tools.git
    cd mpsd-hpc-tools
    flit install -s --deps all

  If you pip is sufficiently new, you can also use that to install the package for development::

    git clone git@gitlab.gwdg.de:mpsd-cs/mpsd-hpc-tools.git
    cd mpsd-hpc-tools
    pip install -e ."[dev]"


Building Debian package
-----------------------

In the package root directory, run `dpkg-buildpackage --no-sign -sa`. After successful build the package can be found in the parent directory and installed using `dpgk -i ../mpsd-hpc-tools_<version>_all.deb`.
By default, every MR will trigger a build of the package in the CI pipeline. The `.deb` package will be uploaded to the  repository as an artifact at the end of the pipeline.
The latest build artifact can be found at `https://gitlab.gwdg.de/mpsd-cs/mpsd-hpc-tools/-/jobs/artifacts/main/raw/artifacts.zip?job=package`.
Remember that every time package version is changed, the `debian/changelog` file needs to be updated accordingly.

Versioning
----------

We use `CalendarVersioning <https://calver.org/>`__ with the YYYY.MM.DD format, i.e. for example ``2023.4.17`` or ``2023.12.31``. If multiple releases per day are necessary, the second overrides the first.




Brainstorming / TODOs
---------------------

Are gathered in `todo.org <todo.org>`__.
