"""Functions that are useful in multiple command line tools."""


def humanise_size(bytes: int) -> str:
    """Convert a number of bytes into a human-readable format.

    The human-readable format return value is a string containing units of MB,
    GB, or TB, and return. With MB we mean MegaByte and imply decimal prefix
    (10^6).

    Examples:

    >>> humanise_size(1_000_000_000)
        '  1.00 GB'

        >>> humanise_size(999_555_000)
        '999.55 MB'

        >>> humanise_size(17_234)
        ' 17.23 KB'

    """
    units = ["B", "KB", "MB", "GB", "TB", "PB", "EB"]

    # Determine the appropriate unit to use
    unit_index = 0
    number = float(bytes)  # compute bytes in units such as KB, MB, etc
    while number >= 1000 and unit_index < len(units) - 1:
        number /= 1000
        unit_index += 1

    size = f"{number:6.2f}"  # 6 digits: 3 numbers + . + 2 postdecimal digits
    unit = units[unit_index]

    return f"{size} {unit}"
