"""mpsd-quota command line tool.

Provides the current use of storage for a user on /home and /scratch .

Options are available to

- display this for another user
- display in units of bytes (for debugging)
- show the version of the mpsd_hpc_tools package
- show some additional output

"""


import argparse
import os
import pathlib
import subprocess
import sys
import textwrap
from typing import Tuple
from mpsd_hpc_tools.lib import humanise_size
from mpsd_hpc_tools import __version__


def get_ceph_attribute(path: str, attribute: str) -> int:
    """Return output of 'getfattr' command.

    Parameters
    ----------
    path : str
        path to ceph managed volume, for example '/scratch/fangohr'
    attribute : str
        getfattrs attribute, for example 'ceph.dir.rbytes' or
        'ceph.quota.max_bytes'

    Returns
    -------
    value : int
        value returned by getfattrs command (only integers support)
    """
    # getfattr command should return a single integer number
    cmd = f"getfattr -n {attribute} --only-values --absolute-names {path}"
    output = subprocess.check_output(cmd.split()).decode()
    return int(output)


def scratch_bytes_used_quota(path: str) -> Tuple[int, int]:
    """
    Given a path (such as '/scratch/fangohr') return the number of bytes used
    and number of bytes available.

    Assumes the /scratch partition is provided by ceph.

    Example:

    >>> scratch_bytes_used_quota("/scratch/fangohr")
    (1780686359, 25000000000000)
    """
    used = get_ceph_attribute(path, "ceph.dir.rbytes")
    quota = get_ceph_attribute(path, "ceph.quota.max_bytes")
    return used, quota


def get_df_output(path: str, output: str) -> int:
    """Return output of 'df --output=OUTPUT PATH' command.

    Parameters
    ----------
    path : str
        path to pass to df command, for example '/home/fangohr'
    output : str
        output understood by df, for example 'used' or 'available'

    Returns
    -------
    value : int
        value returned by df command (only integers support)
    """

    cmd = f"df --output={output} --block-size=KB {path}"
    stdout = subprocess.check_output(cmd.split()).decode()
    # Example call of df:
    """
    $ df --block-size=kB --output=used /home/fangohr
         Used
    4552917kB
    """
    # get rid of "Used" header by looking at the second line
    data = stdout.split()[1]
    # get rid of "kB" by stripping of "kB" from the right
    assert "kB" in data  # sanity check in case df changes its behaviour
    numberstring = int(data.rstrip("kB"))
    number = int(numberstring) * 1000  # kB == 1000
    return number


def home_bytes_used_quota(path: str) -> Tuple[int, int]:
    """
    Return number of bytes used and available in /home .

    Given a path (such as '/home/fangohr') return the number of bytes used
    and number of bytes available based on output of 'df /home/fangohr'.

    Example:

    >>> home_bytes_used_quota("/home/fangohr")
    (4552917000, 102821266000)

    """
    used = get_df_output(path, "used")
    avail = get_df_output(path, "avail")
    return used, avail


def compose_quota_report(homedir=None, scratchdir=None, bytes=False) -> str:
    """Given a home directory and scratch directory, create an overview of disk usage.

    Optional flags 'bytes' to fine tune behaviour to print bytes instead of
    human-readable output.

    Parameters
    ----------

    homedir : str or pathlib.Path or None

    scratchdir : str or pathlib.Path or None

    bytes: bool

    Returns
    -------

    output : str
       multi-line string (see example)

    Example:
    >>> print(compose_quota_report(homedir='/home/fangohr',
              scratchdir='/scratch/fangohr')
    location                       used          avail            use%
    /home/fangohr               4.55 GB      102.82 GB           4.43%
    /scratch/fangohr            1.78 GB       25.00 TB        0.00712%

    """
    format_string = "{:20} {:>14} {:>14} {:>14.4}%"
    out = format_string.format("location", "used", "avail", "use")

    if homedir:
        # convert homedir into pathlib object
        homedir = pathlib.Path(homedir)
        used_bytes, avail_bytes = home_bytes_used_quota(str(homedir))
        rel = used_bytes / (avail_bytes + used_bytes) * 100  # relative usage in percent
        if bytes:
            used, avail = map(str, [used_bytes, avail_bytes])
        else:
            used, avail = map(humanise_size, [used_bytes, avail_bytes])
        out += "\n" + format_string.format(str(homedir), used, avail, rel)

    if scratchdir:
        scratchdir = pathlib.Path(scratchdir)
        used_bytes, avail_bytes = scratch_bytes_used_quota(str(scratchdir))
        rel = used_bytes / (avail_bytes + used_bytes) * 100  # relative usage in percent
        if bytes:
            used, avail = map(str, [used_bytes, avail_bytes])
        else:
            used, avail = map(humanise_size, [used_bytes, avail_bytes])

        out += "\n" + format_string.format(str(scratchdir), used, avail, rel)

    return out


def main(argv=None):
    """Main function for mpsd-quota command.

    Parameters
    ----------

    argv : List[str] or None
        Can provide list of arguments (reflecting sys.argv) for testing purposes
        The default value (None) will result in `sys.argv` being used.
    """
    parser = argparse.ArgumentParser(
        prog="mpsd-quota",
        description="Shows current usage of /home and /scratch",
        epilog=textwrap.dedent(
            """
            Output of mpsd-quota is in multiples of 1000 (i.e. decimal
            prefixes) and not 1024 (i.e. binary prefixes). If you want to
            compare output from mpsd-quota with output from "df -h", then
            use "df -h --si" to enforce use of decimal prefixes by df.
            """
        ),
    )

    # if executed on CI, "USER" may not be defined in environment. Then use
    # "UNKNOWN":
    userdefault = os.environ.get("USER", "UNKNOWN")

    parser.add_argument(
        type=str,
        default=userdefault,
        dest="user",
        nargs="?",
        help=f'quota for which user (by default "{userdefault}")',
    )
    parser.add_argument("--verbose", "-v", help="be more verbose", action="store_true")
    parser.add_argument(
        "--version", "-V", help="display version and exit", action="store_true"
    )
    parser.add_argument(
        "--bytes",
        help="display storage in bytes (default: human readable)",
        action="store_true",
    )

    # if argv==None, then parse_args will use sys.argv (normal scenario) if we
    # are testing `main(argv)` we can parse custom arguments into `argv` (for
    # testing)
    args = parser.parse_args(argv)

    if args.verbose:
        print("Increased verbosity (DEBUG messages)")
        print(f"Producing output for user '{args.user}'")
        print(f"{args=}")

    if args.version:
        print(__version__)
        sys.exit(0)

    # by default the user executing the command, or user provided on command line
    user = args.user

    # check user exists
    homedir = pathlib.Path("/home") / user
    if not homedir.is_dir():
        print(f"Home directory {str(homedir)} does not exist ({user=}).")
        sys.exit(1)

    # do the work

    scratch_path = pathlib.Path("/scratch") / user
    # only proceed if user has a directory on /scratch
    if scratch_path.is_dir():
        scratchdir = scratch_path
    else:
        scratchdir = None

    output = compose_quota_report(
        homedir=homedir, bytes=args.bytes, scratchdir=scratchdir
    )
    print(output)


if __name__ == "__main__":
    main()
