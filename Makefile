style:
	@# Check style of code
	ruff .
	black --check --diff .

fix:
	@# auto fix style of code where possible
	ruff --fix .
	black .

test:
	@# run tests
	py.test -v .

test+coverage:
	@# run tests and provide coverage overview on terminal and in html format
	py.test -v --cov . --cov-report=html --cov-report=term
  
typing:
	@# run static typing check - can reveal bugs / lack of clarity
	mypy .

deb-package-ci:
	@# build a debian package using dpkg-buildpackage
	/usr/lib/pbuilder/pbuilder-satisfydepends
	dpkg-buildpackage --no-sign -sa

	
